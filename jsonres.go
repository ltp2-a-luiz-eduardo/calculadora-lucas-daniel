package main

import (
	"encoding/json"
	"net/http"
)

func SuccessRes(res http.ResponseWriter, resultado float64, operacao string) {
	res.Header().Set("content-Type", "application/json")
	response := map[string]interface{}{
		"code": 200,
		"result": resultado,
	}

	_, ok := operacoesmapa[operacao]
	if !ok {
		return
	}
	response["op"] = operacao
	json.NewEncoder(res).Encode(response)
}

func InvalidOpRes(res http.ResponseWriter, mensagem string) {
	res.WriteHeader(http.StatusBadRequest)
	response := map[string]interface{}{
		"code": 400,
		"result": "expressão invalida",
		"op": mensagem,
	}
	json.NewEncoder(res).Encode(response)
}

func InvalidoPath(res http.ResponseWriter) {
	res.WriteHeader(http.StatusNotFound)
	response := map[string]interface{}{
		"code":  404,
		"error": "not found",
	}
	json.NewEncoder(res).Encode(response)
}

func SendErrorResponse(res http.ResponseWriter) {
	res.WriteHeader(http.StatusInternalServerError)
	response := map[string]interface{}{
		"code":  500,
		"error": "something went wrong",
	}
	json.NewEncoder(res).Encode(response)
}
