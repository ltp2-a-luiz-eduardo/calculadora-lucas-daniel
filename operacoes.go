package main

import (
	"math"
	"net/http"
)

type calculator struct{}

type operacoes interface {
	operacao(a, b float64) float64
}

type add struct{}

func (add) operacao(a, b float64) float64 {
	return a + b
}

type subtracao struct{}

func (subtracao) operacao(a, b float64) float64 {
	return a - b
}

type multiplicacao struct{}

func (multiplicacao) operacao(a, b float64) float64 {
	return a * b
}

type divisao struct{}

func (divisao) operacao(a, b float64) float64 {
	return a / b
}

type potencia struct{}

func (potencia) operacao(a, b float64) float64 {
	return math.Pow(a, b)
}

type raiz struct{}

func (raiz) operacao(a, b float64) float64 {
	return math.Pow(a, 1/b)
}

var operacoesmapa = map[string]operacoes{
	"add": add{},
	"sub": subtracao{},
	"mul": multiplicacao{},
	"div": divisao{},
	"pow": potencia{},
	"rot": raiz{},
}

func (c calculator) FazOp(res http.ResponseWriter, action string, num1, num2 float64) (float64, error) {
	op, ok := operacoesmapa[action]
	if !ok {
		InvalidOpRes(res, "Operação inválida: "+action)
		return 0, nil
	}
	resultado := op.operacao(num1, num2)
	return resultado, nil
}
