package main

import (
	"log"
	"net/http"
	"time"
)

type myhandler struct {
	calculatora *calculator
	parametros  *parametrosURL
}

func (s *myhandler) ServeHTTP(res http.ResponseWriter, req *http.Request) { //essa e a parte onde cuida dos metodos... tipo GET, POST, ETC
	if req.Method != "GET" {
		InvalidoPath(res)
	}
	action, num1, num2, err := s.parametros.ObterPa(res, req)
	if err != nil {
		InvalidOpRes(res, err.Error())
	}

	resultado, err := s.calculatora.FazOp(res, action, num1, num2)
	if err != nil {
		InvalidOpRes(res, err.Error())
	}
	SuccessRes(res, resultado, action)
}

func IniciarServer() {
	cal := &calculator{}
	par := &parametrosURL{}

	server := &http.Server{
		Addr:         "localhost:8080",
		Handler:      &myhandler{calculatora: cal, parametros: par},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Fatal(server.ListenAndServe())
}
