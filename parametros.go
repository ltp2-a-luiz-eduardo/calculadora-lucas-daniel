package main

import (
	"net/http"
	"strconv"
)

type parametrosURL struct{}

func (p *parametrosURL) ObterPa(res http.ResponseWriter, req *http.Request) (string, float64, float64, error) {
	params := req.URL.Query()

	action := params.Get("action")
	num1 := params.Get("num1")
	num2 := params.Get("num2")

	if action == "" || num1 == "" || num2 == "" {
		return "", 0, 0, nil
	}
	num1F, err := strconv.ParseFloat(num1, 64)
	if err != nil {
		return "", 0, 0, nil
	}
	num2F, err := strconv.ParseFloat(num2, 64)
	if err != nil {
		return "", 0, 0, nil
	}

	return action, num1F, num2F, nil
}
